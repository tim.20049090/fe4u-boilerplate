export function searchUsers(searchValue, allUsers) {
    const searchPropertys = ["full_name", "note", "age"];
    const matchingUsers = [];

    for (const user of allUsers) {
      for (const property of searchPropertys) {
        if (user[property] && user[property].toString().includes(searchValue)) {
          matchingUsers.push(user);
          break;
        }
      }
    }
    return matchingUsers;
  }
  