export function formatUsers(additionalUsers,randomUserMock) {

    const formattedArray = randomUserMock.map((item) => {
      return {
        gender: item.gender,
        title: item.name.title,
        full_name: `${item.name.first} ${item.name.last}`,
        city: item.location.city,
        state: item.location.state,
        country: item.location.country,
        postcode: item.location.postcode,
        coordinates: item.location.coordinates,
        timezone: item.location.timezone,
        email: item.email,
        b_date: item.dob.date,
        age: item.dob.age,
        phone: item.phone,
        picture_large: item.picture.large,
        picture_thumbnail: item.picture.thumbnail,
      };
    });
  
    const allUsers = formattedArray.concat(additionalUsers);
    const uniqueUsers = [];

    for (const user of allUsers) {
      const isDuplicate = uniqueUsers.some((uniqueUser) => {
        return (
          uniqueUser.email === user.email &&
          uniqueUser.phone === user.phone
        );
      });
      if (!isDuplicate) {
        uniqueUsers.push(user);
      }
    }
  
    const courses = [
      "Mathematics",
      "Physics",
      "English",
      "Computer Science",
      "Dancing",
      "Chess",
      "Biology",
      "Chemistry",
      "Law",
      "Art",
      "Medicine",
      "Statistics"
    ];
  
    function getRandomCourse() {
      const randomIndex = Math.floor(Math.random() * courses.length);
      return courses[randomIndex];
    }
  
    const processedUsers = uniqueUsers.map((item, index) => {
      return {
        id: index + 1,
        favorite: false,
        course: getRandomCourse(),
        bg_color: "#FFFFFF",
        note: "Your note here ...",
        ...item,
      };
    });
  
    return processedUsers;
  }