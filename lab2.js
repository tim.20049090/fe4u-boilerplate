import {randomUserMock, additionalUsers } from './FE4U-Lab2-mock.js';
import { formatUsers } from './Task1Formating.js';
import { validateUsers } from './Task2Validation.js';
import { filterUsersAnd } from './Task3FilterAnd.js';
import { sortUsers } from './Task4FilterOr.js';
import { searchUsers } from './Task5Search.js';
import { findPersent } from './Task6SearchPersent.js';

const formatedUsers = formatUsers(additionalUsers,randomUserMock);
//I live outpust here if you want to check arrays after each function, bucause terminal do not fit all elements of all arreys(at least mine)
//console.log(formatedUsers);
console.log(`Number of formated users = ${formatedUsers.length}`);

const validatedUsers = validateUsers(formatedUsers);
//console.log(validateObjects(formatedUsers));
console.log(`Number of validated users = ${validatedUsers.length}`);

const defFilters = {
    country: 'any',
    minAge: 18,
    maxAge: 100,
    gender: 'any',
    favorite: 'all',
};

const usersForDefFilters = filterUsersAnd(validatedUsers, defFilters);
console.log(`Number of default filter users = ${usersForDefFilters.length}`); //The same as in validatedUsers, it means that it is all users

const customFilters = {
    country: 'United States',
    minAge: 30,
    maxAge: 65,
    gender: 'male',
    favorite: 'false',
};

const usersForCastomFilters = filterUsersAnd(validatedUsers, customFilters);
console.log(`\nCustom filter users:`)
console.log(usersForCastomFilters);


const sortUsersByInc = sortUsers(validatedUsers, 'age', 'inc');
// Uncomment if you want to see sorted array
//console.log(sortUsersByInc);


const sortUsersByDec = sortUsers(validatedUsers, 'age', 'dec');
// Uncomment if you want to see sorted array
//console.log(sortUsersByInc);


const searchedUsers = searchUsers(28, validatedUsers);
console.log(`\nAll 28 years old users:`)
console.log(searchedUsers);

var persentOfUsers = findPersent('filter', customFilters , validatedUsers);
console.log(`Persent of castom filter users = ${persentOfUsers}`);

persentOfUsers = findPersent('search', 28 , validatedUsers);
console.log(`Persent of 28 years old users = ${persentOfUsers}`);