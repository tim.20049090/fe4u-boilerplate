import { teachers } from './teacherList.js';
import { createTeacherElements } from './teacherDisplay.js';

function searchTeachers(searchValue, allUsers) {
  const searchPropertys = ["full_name", "note", "age"];
  const matchingUsers = [];

  for (const user of allUsers) {
    for (const property of searchPropertys) {
      if (user[property] && user[property].toString().includes(searchValue)) {
        matchingUsers.push(user);
        break;
      }
    }
  }
  return matchingUsers;
}

//this part just for Serch button, because it exist, but it is redundant!
const searchButton = document.getElementById("search_button");

searchButton.addEventListener("click", function() {
  const searchInput = document.getElementById("search");
  const searchValue = searchInput.value;
  const findedTeachers = searchTeachers(searchValue, teachers);
  createTeacherElements(findedTeachers);
});

//For this part button do not needed
const searchInput = document.getElementById("search");

searchInput.addEventListener("input", function() {
  const searchValue = searchInput.value.trim();
  if (searchValue === "") {
    createTeacherElements(teachers);
  } else {
    const foundTeachers = searchTeachers(searchValue, teachers);
    createTeacherElements(foundTeachers);
  }
});
  