import { teachers } from './teacherList.js';
import { updateTeacherDisplay } from './filterTeachers.js';
import { updateFavoriteTeachers } from './favoriteTeacherDisplay.js';

function displayTeacherOverlay(teacher) {
    const teacherInfoContainer = document.getElementById("teacher_info");

    const wrapperDiv = document.createElement("div");
    wrapperDiv.classList.add("wrapper", "info");

    const topLoginDiv = document.createElement("div");
    topLoginDiv.classList.add("top__login");

    const h2 = document.createElement("h2");
    h2.textContent = "Teacher Info";

    const closeLink = document.createElement("a");
    closeLink.classList.add("close_info");
    closeLink.href = "#";
    closeLink.innerHTML = "&times;";
    closeLink.id = "close";

    topLoginDiv.appendChild(h2);
    topLoginDiv.appendChild(closeLink);

    const addFormDiv = document.createElement("div");
    addFormDiv.classList.add("add_form");

    const containerDiv = document.createElement("div");
    containerDiv.classList.add("container");

    const mainInfoDiv = document.createElement("div");
    mainInfoDiv.classList.add("main_info");

    const teacherPhotoImg = document.createElement("img");
    teacherPhotoImg.classList.add("teacher_photo");
    if (teacher.picture_large !== "#") {
        teacherPhotoImg.src = teacher.picture_large;
    } else {
      teacherPhotoImg.src = 'images/User.png';
    }

    const teacherInfoDiv = document.createElement("div");
    teacherInfoDiv.classList.add("teacher-info");

    const h2Teacher = document.createElement("h2");
    h2Teacher.textContent = teacher.full_name;

    const h3Course = document.createElement("h3");
    h3Course.textContent = teacher.course;

    const pLocation = document.createElement("p");
    pLocation.textContent = teacher.city + ", " + teacher.country;

    const pAgeGender = document.createElement("p");
    pAgeGender.textContent = teacher.age + ", " + teacher.gender;

    const aEmail = document.createElement("a");
    aEmail.href = "mailto:" + teacher.email;
    aEmail.textContent = teacher.email;

    const pPhone = document.createElement("p");
    pPhone.textContent = teacher.phone;

    const starDiv = document.createElement("div");
    starDiv.classList.add("star");
    if(!teacher.favorite){
      starDiv.innerHTML = "&#9734;";
    }else{
      starDiv.innerHTML = "&#9733;";
    }
    starDiv.id = "star";

    const bioDiv = document.createElement("div");
    bioDiv.classList.add("bio");

    const pBio = document.createElement("p");
    pBio.textContent = teacher.note;

    const toggleMapLink = document.createElement("a");
    toggleMapLink.href = "#";
    toggleMapLink.textContent = "toggle map";

    teacherInfoDiv.appendChild(h2Teacher);
    teacherInfoDiv.appendChild(h3Course);
    teacherInfoDiv.appendChild(pLocation);
    teacherInfoDiv.appendChild(pAgeGender);
    teacherInfoDiv.appendChild(aEmail);
    teacherInfoDiv.appendChild(pPhone);

    mainInfoDiv.appendChild(teacherPhotoImg);
    mainInfoDiv.appendChild(teacherInfoDiv);
    mainInfoDiv.appendChild(starDiv);

    bioDiv.appendChild(pBio);

    containerDiv.appendChild(mainInfoDiv);
    containerDiv.appendChild(bioDiv);
    containerDiv.appendChild(toggleMapLink);

    addFormDiv.appendChild(containerDiv);

    wrapperDiv.appendChild(topLoginDiv);
    wrapperDiv.appendChild(addFormDiv);

    teacherInfoContainer.appendChild(wrapperDiv);

    const overlay = document.querySelector(".overlay");
    overlay.style.visibility = "visible";
    overlay.style.opacity = "1";
    overlay.style.zIndex = "11";

    const star = document.getElementById("star");
    star.addEventListener("click", function(){
      teacher.favorite = !teacher.favorite;
      if(!teacher.favorite){
        starDiv.innerHTML = "&#9734;";
      }else{
        starDiv.innerHTML = "&#9733;";
      }
      updateTeacherDisplay(teachers);
      updateFavoriteTeachers();
    });

    const closeButton = document.getElementById("close");
    closeButton.addEventListener("click", function () {
      const teacherInfoContainer = document.getElementById("teacher_info");
      while (teacherInfoContainer.firstChild) {
        teacherInfoContainer.removeChild(teacherInfoContainer.firstChild);
      }
      const close_overlay = document.querySelector(".overlay");
      close_overlay.style.visibility = "hidden";
    });
}


export function findTeacherById(event) {
    event.preventDefault();
    const teacherId = event.currentTarget.getAttribute("id");
    const teacher = teachers.find((someTeacher) => `teacherId-${someTeacher.id}` === teacherId);
  
    if (teacher) {
      displayTeacherOverlay(teacher);
    }
}