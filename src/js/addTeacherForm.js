import { teachers } from './teacherList.js';
import { createTeacherElements } from './teacherDisplay.js';

function handleFormSubmit(event) {
    event.preventDefault(); 

    const form = event.target;
  
    const name = form.querySelector('input[type="text"]').value;
    const speciality = form.querySelector('.speciality-select').value;
    const country = form.querySelector('.country-select').value;
    const city = form.querySelector('input[type="text"][placeholder="City"]').value;
    const email = form.querySelector('input[type="email"]').value;
    const phone = form.querySelector('input[type="tel"]').value;
    const birthdate = form.querySelector('.datepicker').value;
    const gender = form.querySelector('input[name="gender"]:checked').value;
    const bgColor = form.querySelector('#bgColor').value;
    const notes = form.querySelector('.notes').value;
  
    const titles = {
      male: 'Mr',
      female: 'Ms',
    };

    const id = teachers[teachers.length - 1].id + 1;
  
    const newTeacher = {
        id: id,
        gender: gender,
        favorite: false,
        course: speciality,
        bg_color: bgColor,
        note: notes,
        title: titles[gender], 
        full_name: name, 
        city: city,
        state: '',
        country: country,
        postcode: '', 
        coordinates: '', 
        timezone: '', 
        email: email,
        b_date: birthdate,
        age: calculateAge(birthdate), 
        phone: phone,
        picture_large: '#', 
        picture_thumbnail: '#',
    }
  
    teachers.push(newTeacher);
    createTeacherElements(teachers);
    form.reset();
  }

function calculateAge(birthdate) {
    const birthDate = new Date(birthdate);
    const currentDate = new Date();
    const ageDifferenceMs = currentDate - birthDate;
    const age = Math.floor(ageDifferenceMs / (365.25 * 24 * 60 * 60 * 1000));
    return age;
}

const addTeacherForm = document.getElementById('add_teacher');
addTeacherForm.addEventListener('submit', handleFormSubmit);
