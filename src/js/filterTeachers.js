import { teachers } from './teacherList.js';
import { createTeacherElements } from './teacherDisplay.js';

export function filterTeachers(allUsers, filters) {
  return allUsers.filter((user) => {
    const isCountryMatch = filters.country === 'any' || user.country === filters.country;
    const isAgeMatch = user.age >= filters.minAge && user.age <= filters.maxAge;
    const isGenderMatch = filters.gender === 'any' || user.gender === filters.gender;
    const isFavoriteMatch = filters.favorite === 'all' || (filters.favorite === 'true' ? user.favorite : !user.favorite);
    const isPhoto = filters.withPhoto === 'all' || user.picture_thumbnail !== "#";

    return isCountryMatch && isAgeMatch && isGenderMatch && isFavoriteMatch && isPhoto;
  });
}

export function updateTeacherDisplay(teachers) {
  const filteredTeachers = filterTeachers(teachers, customFilters);
  createTeacherElements(filteredTeachers);
}

const customFilters = {
  country: 'any',
  minAge: 18,
  maxAge: 100,
  gender: 'any',
  favorite: 'all', 
  withPhoto: 'all',
};

document.getElementById('age').addEventListener('change', (event) => {
  const ageRange = event.target.value.split('-');
  customFilters.minAge = parseInt(ageRange[0]);
  customFilters.maxAge = parseInt(ageRange[1]);

  updateTeacherDisplay(teachers);
});

document.getElementById('region').addEventListener('change', (event) => {
  customFilters.country = event.target.value;

  updateTeacherDisplay(teachers);
});

document.getElementById('sex').addEventListener('change', (event) => {
  customFilters.gender = event.target.value;

  updateTeacherDisplay(teachers);
});

document.getElementById('photoCheckbox').addEventListener('change', (event) => {
  customFilters.withPhoto = event.target.checked ? 'true' : 'all';

  updateTeacherDisplay(teachers);
});

document.getElementById('favoriteCheckbox').addEventListener('change', (event) => {
  customFilters.favorite = event.target.checked ? 'true' : 'all';

  updateTeacherDisplay(teachers);
});

updateTeacherDisplay(teachers);