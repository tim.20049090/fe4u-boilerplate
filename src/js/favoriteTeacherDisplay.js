import { teachers } from './teacherList.js';
import { filterTeachers } from './filterTeachers.js';
import { findTeacherById } from './teacherInfoDisplay.js';
import { generateInitials } from './teacherDisplay.js';

export function createTeacherElements(teachers) {
    const teacherContainer = document.getElementById("favorite__teachers");

    //removing all teachers
    while (teacherContainer.firstChild) {
        teacherContainer.removeChild(teacherContainer.firstChild);
    }

    //adding teachers
    teachers.forEach((teacher) => {
        const teacherDiv = document.createElement("div");
        teacherDiv.classList.add("teacher");

        const boxDiv = document.createElement("div");
        boxDiv.classList.add("box");

        const anchor = document.createElement("a");
        anchor.classList.add("teacher__button-show");
        anchor.href = "#teacher";
        anchor.id = `teacherId-${teacher.id}`;
        anchor.addEventListener("click", findTeacherById);

        const containerWithStar = document.createElement("div");
        containerWithStar.classList.add("container_with_star");

        const imageContainer = document.createElement("div");
        imageContainer.classList.add("image-container");

        const teacherImage = document.createElement("div");
        
        if (teacher.picture_thumbnail === "#") {
            teacherImage.style.backgroundColor = teacher.bg_color || 'gold';
            teacherImage.classList.add("initials_container");
            teacherImage.textContent = generateInitials(teacher.full_name);
        } else {
            const teacherImg = document.createElement("img");
            teacherImg.classList.add("teacher_photo");
            teacherImg.src = teacher.picture_thumbnail;
            teacherImage.appendChild(teacherImg);
        }

        const teacherName = document.createElement("p");
        teacherName.classList.add("teacher_name");
        teacherName.textContent = teacher.full_name;

        const science = document.createElement("p");
        science.classList.add("science");
        science.textContent = teacher.course;

        const location = document.createElement("p");
        location.classList.add("location");
        location.textContent = teacher.country;

        const star = document.createElement("div");
        star.classList.add("indicator_star");
        star.innerHTML = "&#9733;";
        containerWithStar.appendChild(star);

        imageContainer.appendChild(teacherImage);
        containerWithStar.appendChild(imageContainer);

        anchor.appendChild(containerWithStar);
        anchor.appendChild(teacherName);
        anchor.appendChild(science);
        anchor.appendChild(location);
        boxDiv.appendChild(anchor);
        teacherDiv.appendChild(boxDiv);
        teacherContainer.appendChild(teacherDiv);
    });
}

export function updateFavoriteTeachers(){
    const favoriteTeachers = filterTeachers(teachers, customFilters);
    createTeacherElements(favoriteTeachers);
}

const customFilters = {
    country: 'any',
    minAge: 18,
    maxAge: 100,
    gender: 'any',
    favorite: 'true', 
    withPhoto: 'all',
};

const favoriteTeachers = filterTeachers(teachers, customFilters);
createTeacherElements(favoriteTeachers);
