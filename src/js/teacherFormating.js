export function formatUsers(additionalUsers,randomUserMock) {

    const formattedArray = randomUserMock.map((teacher) => {
      return {
        gender: teacher.gender,
        title: teacher.name.title,
        full_name: `${teacher.name.first} ${teacher.name.last}`,
        city: teacher.location.city,
        state: teacher.location.state,
        country: teacher.location.country,
        postcode: teacher.location.postcode,
        coordinates: teacher.location.coordinates,
        timezone: teacher.location.timezone,
        email: teacher.email,
        b_date: teacher.dob.date,
        age: teacher.dob.age,
        phone: teacher.phone,
        picture_large: teacher.picture.large,
        picture_thumbnail: teacher.picture.thumbnail,
      };
    });
  
    const allUsers = formattedArray.concat(additionalUsers);
    const uniqueUsers = [];

    for (const user of allUsers) {
      const isDuplicate = uniqueUsers.some((uniqueUser) => {
        return (
          uniqueUser.email === user.email &&
          uniqueUser.phone === user.phone
        );
      });
      if (!isDuplicate) {
        uniqueUsers.push(user);
      }
    }
  
    const courses = [
      "Mathematics",
      "Physics",
      "English",
      "Computer Science",
      "Dancing",
      "Chess",
      "Biology",
      "Chemistry",
      "Law",
      "Art",
      "Medicine",
      "Statistics"
    ];
  
    function getRandomCourse() {
      const randomIndex = Math.floor(Math.random() * courses.length);
      return courses[randomIndex];
    }
  
    const processedUsers = uniqueUsers.map((item, index) => {
      return {
        id: index + 1,
        favorite: false,
        course: getRandomCourse(),
        bg_color: "#FFFFFF",
        note: "Your note here ...",
        ...item,
      };
    });
  
    return processedUsers;
  }