import { teachers } from './teacherList.js';

export function generateTable(allTeachers) {
    const tableContainer = document.getElementById("table_rows");
    while (tableContainer.firstChild) {
        tableContainer.removeChild(tableContainer.firstChild);
    }

    allTeachers.forEach((teacher) => {
        const tableRow = document.createElement("tr");
        tableRow.innerHTML = `<td>${teacher.full_name}</td><td>${teacher.course}</td><td>${teacher.age}</td><td>${teacher.gender}</td><td>${teacher.country}</td>`;
        tableContainer.appendChild(tableRow);
    });
}


generateTable(teachers.slice(0,10));
