import { randomUserMock, additionalUsers } from './FE4U-Lab2-mock.js';
import { formatUsers } from './teacherFormating.js';
import { validateUsers } from './teacherValidation.js';

const formatedUsers = formatUsers(additionalUsers,randomUserMock);
export const teachers = validateUsers(formatedUsers);