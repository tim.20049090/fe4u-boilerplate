import { teachers } from './teacherList.js';
import { generateTable } from './tableDisplay.js';

export function sortUsers(allUsers, property, order = 'inc') {
  if (!['full_name', 'age', 'course', 'gender', 'country'].includes(property)) {
      throw new Error('Invalid property specified. Allowed values are: full_name, age, b_day, country');
  }
  return allUsers.slice().sort((a, b) => {
    const valueA = a[property];
    const valueB = b[property];
    if (valueA < valueB) {
      return order === 'inc' ? -1 : 1;
    } else if (valueA > valueB) {
      return order === 'inc' ? 1 : -1;
    } else {
      return 0;
    }    
  });
}

const sortOrders = {};

function sortTable(property) {
  const currentOrder = sortOrders[property] || "inc";
  const sortedUsers = sortUsers(teachers, property, currentOrder);
  generateTable(sortedUsers.slice(0,10));
  sortOrders[property] = currentOrder === "inc" ? "dec" : "inc";
}

const tableHeaders = document.querySelectorAll(".statistics_header");

tableHeaders.forEach(tableHeader =>{
  tableHeader.addEventListener("click", (event) => {
    const propertyId = event.target.id;
    const propertyName = propertyId.split("-sort")[0];

    const currentText = tableHeader.textContent;
    tableHeaders.forEach(someHeader => {
      if (someHeader !== tableHeader) {
          someHeader.textContent = someHeader.textContent.replace(' ↑', '').replace(' ↓', '');
      }
    });
    if (currentText.includes('↑')) {
      tableHeader.textContent = currentText.replace(' ↑', ' ↓');
    } else if (currentText.includes('↓')) {
        tableHeader.textContent = currentText.replace(' ↓', ' ↑');
    } else {
        tableHeader.textContent = currentText + ' ↑';
    }

    sortTable(propertyName);
  })
});


