export function sortUsers(allUsers, property, order = 'inc') {
    if (!['full_name', 'age', 'b_day', 'country'].includes(property)) {
        throw new Error('Invalid property specified. Allowed values are: full_name, age, b_day, country');
    }
    return allUsers.slice().sort((a, b) => {
        const dateA = new Date(a.b_day);
        const dateB = new Date(b.b_day);
        const valueA = a[property];
        const valueB = b[property];
    
        if (dateA < dateB) {
          return order === 'inc' ? -1 : 1;
        } else if (dateA > dateB) {
          return order === 'inc' ? 1 : -1;
        } else {
          if (valueA < valueB) {
            return order === 'inc' ? -1 : 1;
          } else if (valueA > valueB) {
            return order === 'inc' ? 1 : -1;
          } else {
            return 0;
          }
        }
    });
}
