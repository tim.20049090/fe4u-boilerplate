function isValidEmail(email) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
return emailRegex.test(email);
}

function isValidStringUp(value) {
    return typeof value === 'string' && /^[A-Z]/.test(value);
}

function isValidString(value) {
    return typeof value === 'string';
}

function isValidNumber(value) {
    return typeof value === 'number';
}

function validateUser(User) {
    return (
        isValidStringUp(User.full_name) &&
        isValidStringUp(User.note) &&
        isValidStringUp(User.state) &&
        isValidStringUp(User.city) &&
        isValidStringUp(User.country) &&
        isValidString(User.gender) &&
        isValidNumber(User.age) &&
        isValidEmail(User.email)
    );
}

export function validateUsers(allUsers) {
    return allUsers.filter((User) => validateUser(User));
}