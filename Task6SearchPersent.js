import { filterUsersAnd } from './Task3FilterAnd.js';
import { searchUsers } from './Task5Search.js';

export function findPersent(type, value, allUsers){
    const numberOfUsers = allUsers.length;
    if (type === 'search'){
        const searchedUsers = searchUsers(value, allUsers);
        return (searchedUsers.length / numberOfUsers * 100).toFixed(2);
    }else if (type === 'filter'){
        const filteredUsers = filterUsersAnd(allUsers, value); //In this case value = customFilters
        return (filteredUsers.length / numberOfUsers * 100).toFixed(2);
    } return 0;
}