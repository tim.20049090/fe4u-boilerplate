export function filterUsersAnd(allUsers, filters) {
  return allUsers.filter((user) => {
    const isCountryMatch = filters.country === 'any' || user.country === filters.country;
    const isAgeMatch = user.age >= filters.minAge && user.age <= filters.maxAge;
    const isGenderMatch = filters.gender === 'any' || user.gender === filters.gender;
    const isFavoriteMatch = filters.favorite === 'all' || (filters.favorite === 'true' ? user.favorite : !user.favorite);

    return isCountryMatch && isAgeMatch && isGenderMatch && isFavoriteMatch;
  });
}